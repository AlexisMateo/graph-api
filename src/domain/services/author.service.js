import { findAuhtorsByBookIds } from '../../repository/author.repository';
import {groupBy, map } from 'ramda';
import DataLoader from 'dataloader';

export class AuhtorService{

    async auhtorsByBookIds(ids){

        let authors = await findAuhtorsByBookIds(ids);

        const authorById = groupBy(author => author.book_authors[0].book_id, authors);
        
        let result = map(id=> {

            return authorById[id]
        
        },ids);

        return result;
    }
}

export function findAuhtorsByBookIdsLoader(){
    return new DataLoader(new AuhtorService().auhtorsByBookIds);
}