import { allBooks, bookById, bookByIds } from '../../repository/book.repository';
import {groupBy, map } from 'ramda';
import DataLoader from 'dataloader';

export class BookService{

    Get(args){
        return allBooks(args);
    }

    GetById(id){
        let book = bookById(id);
        return book;
    }

    async GetByIds(ids){
        let books = await bookByIds(ids);
        let booksGrouped = groupBy(book=>book.id, books);
        let mappedBooks = map(id=>{
          const book =  booksGrouped[id]? booksGrouped[id][0]:null;
            return book;
        },ids);
        return mappedBooks;
    }

    imageUrl(size, id){
       const zoom = size == 'SMALL' ? 1 : 0;
       return `https://books.google.com/books/content?id=${id}&printsec=frontcover&img=1&zoom=${zoom}&source=gbs_api` 
    }   
}


export function findBookIdsLoader(){
    return new DataLoader(new BookService().GetByIds);
}