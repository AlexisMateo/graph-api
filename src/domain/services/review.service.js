import {getAllReviews, getReviewByBookId, createReview} from '../../repository/review.repository'
import DataLoader from 'dataloader';
import {groupBy, map } from 'ramda';

export default class ReviewService{
    reviews(sortBy){
        return getAllReviews(sortBy);
    }
    async reviewsByBookIds(bookIds){
        try {
            const reviews = await getReviewByBookId(bookIds);
            const reviewsGrouped = groupBy(review=>review.bookId, reviews);
            return map(id=>reviewsGrouped[id], bookIds);

        } catch (error) {
            console.error(error);
            throw error;
        }

    }

    async create(reviewInput){
        return await createReview(reviewInput);
    }
}

export function findReviewsByBookIdLoader(){
    return new DataLoader(new ReviewService().reviewsByBookIds);
}