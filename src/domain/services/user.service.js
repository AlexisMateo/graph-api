import { userById } from '../../repository/user.repository';
import DataLoader from 'dataloader';
import {groupBy, map } from 'ramda';

export class UserService{

    async GetById(ids){
        let users =  await userById(ids);
        let groupUsers = groupBy(user=>user.id,users);
        let mapUsers = map(id => {
            let user = groupUsers[id]?groupUsers[id][0]:null;
            return user
        },ids);
        
        return mapUsers;
    }  

}


export function findUserLoader(){
    return new DataLoader(new UserService().GetById);
}