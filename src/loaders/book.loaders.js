import {findBookIdsLoader} from '../domain/services/book.service';

export default {
    findBookIdsLoader:findBookIdsLoader()
}