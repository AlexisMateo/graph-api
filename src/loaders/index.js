import authorLoader from '../loaders/author.loaders';
import bookLoader from '../loaders/book.loaders';
import userLoader from '../loaders/user.loader';
import reviewLoader from '../loaders/review.loaders';

export default ()=>({
    ...authorLoader,
    ...bookLoader,
    ...userLoader,
    ...reviewLoader
})
