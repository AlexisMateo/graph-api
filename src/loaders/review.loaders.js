import {findReviewsByBookIdLoader} from '../domain/services/review.service';

export default {
    findReviewsByBookIdLoader:findReviewsByBookIdLoader()
}