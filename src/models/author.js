/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Author =  sequelize.define('author', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    tokens: {
      type: "TSVECTOR",
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.fn('now')
    }
  }, {
    tableName: 'author',
    timestamps: false,
    underscored: true
  });

  Author.associate = models => {
    Author.hasMany(models.book_author);
  };

  return Author;
};
