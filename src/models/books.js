/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('books', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    googleId: {
      type: DataTypes.TEXT,
      allowNull: true,
      unique: true,
      field:'google_id'
    },
    title: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    subtitle: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    pageCount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '0',
      field:'page_count'
    },
    ratingTotal: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '0',
      field:'rating_total'
    },
    ratingCount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: '0',
      field:'rating_count'
    },
    rating: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: '0'
    },
    tokens: {
      type: "TSVECTOR",
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.fn('now'),
      field:'created_at'

    }
  }, {
    tableName: 'books',
    timestamps: false
  });
};
