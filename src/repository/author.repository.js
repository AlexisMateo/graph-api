import db from '../models';

export async function findAuhtorsByBookIds(ids){
    try{
        const authors= await db.author.findAll({
            include:[
                {
                    model:db.book_author,
                    attributes:['book_id'],
                    where:{
                        book_id : {
                            $any:ids
                        }
                    }
                }]
        });

        return authors;

    }catch(err){
        console.log(err);
        throw err;
    }
}