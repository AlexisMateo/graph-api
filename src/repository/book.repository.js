import db from '../models';

const ORDER_BY={
    ID_DESC:"id",
    RATING_DESC:"rating"
}

export async function allBooks(sorting){
    try{
        let orderByColumn = ORDER_BY[sorting.orderBy];

        const books = await db.books.findAll({
            order: [
                [orderByColumn, 'DESC']
            ],
        });
        return books;
        
    }catch(err){
        console.error(err)
        throw err;
    }
    //TODO: Query books from DB
}

export async function bookById(bookId){
    try{
       
        const book = await db.books.findAll({
            where: {
              id: bookId
            }
          });
        
        return book[0];
        
    }catch(err){
        console.error(err)
        throw err;
    }
    //TODO: Query books from DB
}

export function bookByIds(ids){
   let books = db.books.findAll({
        where:{
                id:{
                    $any:ids
                }
            }
        });
    return books;
}