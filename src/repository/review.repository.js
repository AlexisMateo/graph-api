import db from '../models';

const ORDER_BY = {
    ID_DESC:'desc',
    ID_ASC:'asc'
}

export async function getAllReviews(sorting){
    let order = ORDER_BY[sorting.orderBy];

     return await db.review.findAll({
         order:[
            ['id', order]
         ]
     });
}

export async function getReviewByBookId(bookIds){
    let reviews = db.review.findAll({
        where:{
            bookId:{
                    $any:bookIds
                }
            }
        });
    return reviews;
}

export async function createReview(reviewInput){
    return await db.review.create({
        ...reviewInput
    });
}