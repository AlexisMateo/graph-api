import db from '../models';

export async function userById(userIds){
    try{

        let users = db.user.findAll({
            where:{
                    id:{
                        $any:userIds
                    }
                }
            });
        return users;
        
    }catch(err){
        console.error(err)
        throw err;
    }
}