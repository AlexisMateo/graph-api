import {BookService}  from './domain/services/book.service';
import ReviewService  from './domain/services/review.service';
import {UserService}  from './domain/services/user.service';

import gravatar from 'gravatar';

const Book = new BookService();
const Review = new ReviewService();
const User = new UserService();

const resolvers = {
    User:{
        imageUrl:(user,{size})=>{
            return gravatar.url(user.email,{size:size})
        }
    },
    Book:{
        imageUrl:(book, {size})=>{
            return Book.imageUrl(size, book.googleId)
        },
        authors: (book, args, context) => {
            const { loaders:{findAuhtorsByBookIdsLoader} } = context;
            const authors = findAuhtorsByBookIdsLoader.load(book.id);
            return authors;
        },
        reviews:(book,args,context)=>{
            const { loaders:{findReviewsByBookIdLoader} } = context;
            const reviews = findReviewsByBookIdLoader.load(book.id);
            return reviews;
        }
    },

    Review:{
        book: (review, args, context) => {
            const { loaders:{findBookIdsLoader} } = context;
            const books = findBookIdsLoader.load(review.bookId);
            return books;
        },
        user:(review, args, context)=>{
            const {loaders:{findUserLoader}} = context;
            const user = findUserLoader.load(review.userId);
            return user;
        }
    },

    Query:{
        books:(root,args)=>{
            return Book.Get(args);
        },
        reviews:(root, args)=>{
            return Review.reviews(args);
        },
        book:(root,{id})=>{
            return Book.GetById(id);
        }
    },
    Mutation:{
        createReview:(root,args)=>{
            const {reviewInput}=args;
            return Review.create(reviewInput);
        }
    }
}

export default resolvers;